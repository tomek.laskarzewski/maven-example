package com.tl.mav.rest.component.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class UnknownUserException extends ResponseStatusException {
  public UnknownUserException(String name) {
    super(HttpStatus.NOT_FOUND, String.format("Unknown user '%s'", name));
  }
}
