package com.tl.mav.rest.component;

import com.tl.mav.rest.model.StatusInfo;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/app")
@RequiredArgsConstructor
public class ResAppController {

  private final ChatService chatService;

  @GetMapping("")
  public String get() {
    return "Waiting for commands";
  }

  @GetMapping("/{name}/connect")
  public StatusInfo start(@PathVariable String name) {
    return chatService.connect(name);
  }

  @GetMapping("/{name}/status")
  public StatusInfo stop(@PathVariable String name) {
    return chatService.status(name);
  }

  @GetMapping("/{name}/disconnect")
  public StatusInfo status(@PathVariable String name) {
    return chatService.disconnect(name);
  }

}
