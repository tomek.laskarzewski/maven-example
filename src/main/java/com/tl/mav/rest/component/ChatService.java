package com.tl.mav.rest.component;

import com.tl.mav.rest.model.Status;
import com.tl.mav.rest.model.StatusInfo;
import com.tl.mav.rest.component.exception.UnknownUserException;
import com.tl.mav.rest.component.exception.UserConnectedException;
import java.time.Clock;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import org.springframework.stereotype.Service;

@Service
public class ChatService {

  private final Map<String, Long> names = new HashMap<>();

  public StatusInfo connect(String name) {
    Long timestamp = names.putIfAbsent(name, timestamp());
    if (Objects.nonNull(timestamp)) {
      throw new UserConnectedException(name, elapsedTimeSeconds(timestamp));
    }

    return StatusInfo.builder()
        .success(true)
        .name(name)
        .status(Status.CONNECTED)
        .activeTimeSeconds(0L)
        .build();
  }

  public StatusInfo disconnect(String name) {
    return status(name, names.remove(name), Status.DISCONNECTED);
  }

  public StatusInfo status(String name) {
    return status(name, names.get(name), Status.CONNECTED);
  }

  private StatusInfo status(String name, Long timestamp, Status onSuccess) {
    if (Objects.isNull(timestamp)) {
      throw new UnknownUserException(name);
    }
    return StatusInfo.builder()
        .success(true)
        .name(name)
        .status(onSuccess)
        .activeTimeSeconds(elapsedTimeSeconds(timestamp))
        .build();
  }

  private long timestamp() {
    return Clock.systemUTC().millis();
  }

  private long elapsedTimeSeconds(long timestamp) {
    return (timestamp() - timestamp) / 1000L;
  }
}
