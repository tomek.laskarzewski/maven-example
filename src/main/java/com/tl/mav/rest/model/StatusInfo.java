package com.tl.mav.rest.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
@JsonInclude(Include.NON_NULL)
public class StatusInfo {

  private boolean success;
  private String errorMessage;
  private Status status;
  private String name;
  private Long activeTimeSeconds;
}
