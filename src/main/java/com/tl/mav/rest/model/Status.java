package com.tl.mav.rest.model;

public enum Status {
  CONNECTED, DISCONNECTED
}
